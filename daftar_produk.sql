-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 01 Feb 2021 pada 16.14
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `produk`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `daftar_produk`
--

CREATE TABLE `daftar_produk` (
  `no` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `stock` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `daftar_produk`
--

INSERT INTO `daftar_produk` (`no`, `harga`, `nama_produk`, `stock`, `jumlah`) VALUES
(1, 7000, 'Baju', 10, 10),
(2, 5000, 'Celana', 15, 15),
(3, 25000, 'Sepatu', 5, 5),
(4, 20000, 'Tas', 7, 7),
(5, 16000, 'Alat Tulis', 8, 8);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `daftar_produk`
--
ALTER TABLE `daftar_produk`
  ADD PRIMARY KEY (`no`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `daftar_produk`
--
ALTER TABLE `daftar_produk`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
