<?php
include 'function.php';

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Halaman Admin</title>
</head>
<body>
	<h1>Daftar Produk</h1>
	<table border="1" cellpadding="10" cellspacing="0">
		<tr>
			<th>No.</th>
			<th>Harga</th>
			<th>Nama_Produk</th>
			<th>Stock</th>
			<th>Jumlah</th>
		</tr>
	
	
		<tr>
	<?php while ($rows = mysqli_fetch_assoc($result) ) : ?>

			<td><?php echo $rows["no"] ?></td>
			<td><?php echo rupiah ($rows["harga"]) ;?></td>
			<td><?php echo $rows["nama_produk"] ?></td>
			<td><?php echo $rows["stock"]; ?></td>
			<td><?php echo $rows["jumlah"]; ?></td>
		</tr>
	<?php endwhile ?>
	</table>
</body>
</html>